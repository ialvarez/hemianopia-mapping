function Output = HemiaMap_Buttonbox(Parameters, Action)
% HemiaMap_Buttonbox(Parameters, Action)
%
% OPTIONS
%   Parameters = HemiaMap_Buttonbox(Parameters, 'open')
%   State      = HemiaMap_Buttonbox(Parameters, 'read')
%   []         = HemiaMap_Buttonbox(Parameters, 'waitfortrigger')
%
% Depending on input, do one of the following:
%   1) 'open' the connection to a USB buttonbox, saving the index to Parameters
%   2) 'read' the state of all buttons, outputs a vector
%   3) 'waitfortrigger' will wait until a signal is detected on button 5
%
% Two sections, one for UNIX (using Gamepad) and one for Windows PC
% (using IOport).
%
% Changelog
%
% 04/12/2018    Lifted from ParamDisp_Buttonbox
%
% Ivan Alvarez
% FMRIB, University of Oxford

%% UNIX

% Optional
if isunix
    
    % Various options
    switch Action
        
        % Open the connection
        case 'open'
            
            % Check buttonbox is present
            Gamepad('Unplug');
            Npads = Gamepad('GetNumGamepads');
            
            % Error checking
            if Npads == 0
                error('No buttonbox detected. If USB is plugged in, try restarting Matlab.');
            end
            
            % How many buttons
            Nbuttons = Gamepad('GetNumButtons', Npads);
            
            % Save values
            Parameters.Buttonbox = Npads;
            Parameters.Nbuttons = Nbuttons;

            % To output
            Output = Parameters;

            % Read the current state
        case 'read'
            
            % Read buttonbox
            ButtonState = zeros(1, Parameters.Nbuttons);
            for b = 1:Parameters.Nbuttons
                ButtonState(b) = Gamepad('GetButton', Parameters.Buttonbox, b);
            end
            
            % To output
            Output = ButtonState;
            
            % Wait for button 5
        case 'waitfortrigger'
            
            % Loop
            while Inf
                
                % Scanner signal comes through button 5
                Output = Gamepad('GetButton', Parameters.Buttonbox, 5);
                
                % Escape if trigger button was pressed
                if Output ~= 0
                    break
                end
            end
            
            % Wait for any button press
        case 'waitforpress'
            
            while Inf
                ButtonState = Gamepad('GetButton', Parameters.Buttonbox, 5);
                if ButtonState ~= 0
                    break
                end
            end
    end
end

%% Windows PC

% Optional
if ispc
    
    % Various options
    switch Action
        
        % Open the connection
        case 'open'
            
            % Open IO port
            Parameters.ioObj = IOport_open;
            
            % Define buttons
            Parameters.IO = IOport_logic;
            
            % FMRIB standard LPT1/parallel port address
            % FMRIB: 0x378 data, 0x379 status, 0x37a control
            Parameters.IOaddress = hex2dec('379');
            
            % To output
            Output = Parameters;
            
            % Read the current state
        case 'read'
            
            % Read bytes
            byte_in = io64(Parameters.ioObj, Parameters.IOaddress);
            
            % Go over each button (first byte is inverted)
            Output(1) = ~bitget(byte_in, Parameters.IO.buttonBit(1));
            Output(2) = bitget(byte_in,  Parameters.IO.buttonBit(2));
            Output(3) = bitget(byte_in,  Parameters.IO.buttonBit(3));
            Output(4) = bitget(byte_in,  Parameters.IO.buttonBit(4));
            Output(5) = bitget(byte_in,  Parameters.IO.trigBit);
            
            % Wait for button 5
        case 'waitfortrigger'
            
            % Start at dummy zero
            byte_in = 0;
            
            % Loop
            while ~bitget(byte_in, Parameters.IO.trigBit)
                
                % Read port
                byte_in = io64(Parameters.ioObj, Parameters.IOaddress);
                WaitSecs(0.005);
            end
    end
end

% Done
%