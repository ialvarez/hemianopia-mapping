function Parameters = HemiaMap_Global
% Parameters = HemiaMap_Global
%
% DESCRIPTION
%   Hard-coded parameters for hemianopia mapping experiment.
%   
% CHANGELOG
%   27/11/2018  Written
%   30/11/2018  Overhaul of fixation task
%   03/12/2018  Added orthogonal dot motion direction
%   13/12/2018  Added task feedback
%   06/02/2019  Removed EPI TR specification, not used
%   11/02/2011  Split welcome and ready on-screen messages
%
% Ivan Alvarez
% FMRIB, University of Oxford
%

%% Parameters

% Directories
Parameters.StimDir = '/Users/ialvarez/Dropbox/Shared/Stimulation/IvanStim/Projects/HemianopiaMapping/ScannerStim/Stimuli';
Parameters.SaveDir = '/Users/ialvarez/Dropbox/Shared/Stimulation/IvanStim/Projects/HemianopiaMapping/ScannerStim/Results';

% Display settings
Parameters.Monitor = 1;            % Secondary monitor
Parameters.InvertDisplay = 0;      % Invert display up/down + left/right for mirror display
Parameters.RefreshRate = 60;       % Hz

% PTB settings
Parameters.Multisample = 0;         % Multisampling anti-alias parameter
Parameters.Gamma = [];              % Gamma correction for 3T BOLDscreen, RGB triplet
Parameters.FontName = 'Arial';
Parameters.FontSize = 60;
Parameters.Black = [0 0 0];
Parameters.White = [1 1 1];
Parameters.Gray = [0.5 0.5 0.5];

% Welcome message
Parameters.WelcomeMessage = 'Please fixate on central dot\n\n\n';
Parameters.ReadyMessage = 'Please fixate on central dot\n\n\nScan about to start';

% Design
% HCP stimuli have 4500 frames and we show each for 4 monitor flips. On a
% 60Hz display, that is 15 frames per second, for a total display duration
% of 300 s, or 5:00 minutes exactly.
Parameters.Nframes = 4500;                  % Number of movie frames in HCP stimulus
Parameters.FrameDuration = 4;               % Number of monitor refreshes for one movie frame

% Motion stimulus
Parameters.DotNum = 7500;                   % Number of dots
Parameters.DotSpeed = 18;                   % Pixels per frame
Parameters.DotSize = 9;                     % Radius in pixels (ParamDisp = 9 pix)
Parameters.DotLifespan = Inf;               % How long each dots remains on the screen
Parameters.DotCoherent = 0.5;               % Fraction of dots with coherent motion
Parameters.DotDirection = 'orthogonal';     % 'lr', 'ud', 'orthogonal' (for coherent dots)
Parameters.DotResolution = 2;               % round

% Fixation dot
Parameters.FixDotSize = 12;                 % Radius in pixels (REF=12, C003 = 72)
Parameters.FixDotAlpha = 1;                 % Transparency of dot

% Task
Parameters.Task = 'swap';                   % 'flash' for brief color change
                                            % 'swap' for static color change
Parameters.TaskHitTolerance = 1;            % Tolerance for accepting a response as a hit after stimulus onset, in seconds

% Task options
switch Parameters.Task
    case 'flash'
        Parameters.TaskEventDuration = 0.4;     % In seconds
        Parameters.TaskEventSpacing = 8;        % One event every N seconds, � jitter
        Parameters.TaskEventJitter = 2;         % Shift event spacing by � N seconds 
        Parameters.TaskColor = [0 0 1; 1 0 0];  % Blue (rest), red (event)
    case 'swap'
        Parameters.TaskEventSpacing = 3;         % One event every N seconds, � jitter
        Parameters.TaskEventJitter = 2;              % Shift event spacing by � N seconds
        Parameters.TaskColor = [1 0 0; 0 0 1; 1 1 0]; % Red, blue, yellow
end

% Eyetracking
Parameters.Et = 1;      % on/off

% Default subject
Parameters.Subject = 'demo';

% Done
%