%% HemiaMap.m
%
% DESCRIPTION
%   pRF mapping stimuli designed for hemianopia mapping experiment.
%   Two stimulus classes available: 
%     a) a 'retino' stimulus using the HCP retinotopy stimulus images with
%        semantic images on a noisy background.
%     b) a 'motion' stimulus consisting of coherent and incorehently moving
%        dots
%   Both stimuli are shown though a moving bar aperture, lifted from the 
%   HCP protocol. This script allows a fully interactive call to either 
%   stimulus.
%
% Some technical notes
%   * The HCP stimulus has a native resolution of 768 x 768
%   * These stimuli have to be shown on a 60 Hz display. Using a slower 
%       (or faster) refresh rate will cause things to break.
%   
% CHANGELOG
%   27/11/2018  Written
%   07/01/2018  Added artificial scotoma option
%               Condition and artifical scotoma options saved to Parameters
%
% Ivan Alvarez
% FMRIB, University of Oxford
%

%% Settings

% Pull global parameters
Parameters = HemiaMap_Global;

%% User input

% Subject code
Subject = inputdlg('Subject: ', 'Subject');
Parameters.Subject = Subject{1};

% Condition
Condition = questdlg('Select condition', 'Menu', ... % Top-level labels
    'retino', 'motion', ... % Options
    'retino'); % Default

% Artificial scotoma
Scotoma = questdlg('Artificial scotoma?', 'Menu', ... % Top-level labels
    'No', 'LVF', 'RVF', ... % Options
    'No'); % Default

% Store options
Parameters.Condition = Condition;
Parameters.Scotoma = Scotoma;

%% Run

% Go!
HemiaMap_Controller(Parameters);

% Done
%