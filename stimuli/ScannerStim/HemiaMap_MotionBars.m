function Behaviour = HemiaMap_MotionBars(Parameters, Win, Task)
%  Behaviour = HemiaMap_MotionBars(Parameters, Win, Task)
%
% Inputs
%   Parameters    [struct] As generated by HemiaMap_Global
%   Win           [scalar] From PTB window launch
%   Task          [struct] As generated by HemiaMap_GenTask
%
% Display motion stimulus, only the bar apertures.
%
% Stimulus data from http://kendrickkay.net/analyzePRF
%
% CHANGELOG
% 27/11/2018    Base layer set
% 28/11/2018    Implemented aperture loading, dot generation
% 29/11/2018    De-bugged dot display, fixed coordinate arguments
%               Added grid
% 30/11/2018    Updated HemiaMap_LoadStim output order
%               Resolution now stored in Parameters
%               Fixed coherent/random motion display
% 03/12/2018    ApIdx now contains information about the bar orientation
%               Replaced separate black/white dot drawing with a single
%               command
% 04/12/2018    Aperture search now performed to the nearest dot pixel
%               position
%               Moved buttonbox interactions to HemiaMap_Buttonbox
% 13/12/2018    Re-wrote frame timekeeping, now handles dropped frames
% 07/01/2019    Added artificial scotoma
% 11/02/2019    Scotoma mask now extends to the lateral edge of the screen
%               Welcome text goes yellow when ready to trigger
% 12/02/2019    Fixed scotoma mask bug, was not extending fully
% 22/02/2019    Only draw a frame if the expected framerate has elapsed,
%               fixes jitter-y display in STIM1
% 05/03/2019    Set DrawTexture FilterMode to 0 for faster drawing
%               Moved Flip() call to the end of the loop for less overhead
%
% Ivan Alvarez
% FMRIB, University of Oxford
%

%% Settings

% How long to show each frame for, in seconds
FrameDur = (1 / Parameters.RefreshRate) * Parameters.FrameDuration;

% Experiment length
Fps = Parameters.RefreshRate / Parameters.FrameDuration; % Frames per second
TotalDur = Parameters.Nframes / Fps; % Stimulus duration, in seconds

% Centre of display
CentreDisp = Parameters.Resolution / 2;

%% Load

% Message
disp('Start: Loading apertures...')

% Load (takes 20 seconds)
[Grid, ApFrm, ApIdx] = HemiaMap_LoadStim(Parameters);

%% Make dots

% Message
disp('Start: Making dots...')

% Make dots
Dots = HemiaMap_GenDots(Parameters, ApIdx);

%% Make textures

% Grid texture
GridTexture = Screen('MakeTexture', Win, Grid);

% Artificial scotoma image
M = permute(Parameters.Gray, [3 1 2]);
M = repmat(M, Parameters.Resolution(2), Parameters.Resolution(1));
M(:, :, 4) = 0;
M(:, 1 : size(M, 2) / 2, 4) = 1;

% Artifical scotoma texture
switch Parameters.Scotoma
    case 'No'
        ScotomaTexture = [];
    case 'LVF'
        ScotomaTexture = Screen('MakeTexture', Win, M);
    case 'RVF'
        ScotomaTexture = Screen('MakeTexture', Win, fliplr(M));
end

%% Behaviour record

% Start empty matrix
Behaviour.EventTime = Task.EventTime;
Behaviour.Response = [];
Behaviour.ResponseTime = [];

%% Ready screen

% Draw grid
Screen('DrawTexture', Win, GridTexture);

% Draw ready text
DrawFormattedText(Win, Parameters.ReadyMessage, 'center', 'center', Parameters.White);

% Draw fixation point
Screen('DrawDots', Win, [0, 0], Parameters.FixDotSize, [Parameters.Black, Parameters.FixDotAlpha], Parameters.Resolution / 2, Parameters.DotResolution);

% Flip
Screen('Flip', Win);

%% Wait for trigger

% Message
disp('%%%% Waiting for trigger %%%%')

% Wait for button 5
HemiaMap_Buttonbox(Parameters, 'waitfortrigger');

% Message to EyeLink
if Parameters.Et
    Eyelink('Command', 'record_status_message ''SCANNER_TRIGGER''');
    Eyelink('Message', 'SCANNER_TRIGGER');
end

%% Display

% Message
disp('%%%% Displaying %%%%')

% Clocks
StartTime = GetSecs;
CurrTime = GetSecs - StartTime;

% Frames
FrameTimes = FrameDur : FrameDur : TotalDur;
LastFrame = 0;
CurrFrame = 0;

% Loop
while CurrTime < TotalDur
    
    %% Clocks
    
    % Update clocks
    CurrTime = GetSecs - StartTime;
    
    % Find nearest frame
    [~, CurrFrame] = min(abs(FrameTimes - CurrTime));
    
    %% Draw if we have a new frame
    
    % Check if we need to draw a new frame
    if CurrFrame > LastFrame
        
        % Update frame counter
        LastFrame = CurrFrame;
        
        %% Background
        
        % Grey background
        Screen('FillRect', Win, Parameters.Gray);
        
        %% Draw dots
        
        % Only if its a stimulation frame
        if ApIdx(CurrFrame) ~= 0
            
            % Pull dot locations
            X = Dots.x(:, CurrFrame);
            Y = Dots.y(:, CurrFrame);
            
            % Pull aperture as indices
            [Ay, Ax] = find(ApFrm(:, :, CurrFrame));
            
            % Find dots that fall inside the aperture (to nearest pixel)
            Inside = ismember([round(X), round(Y)], [Ax, Ay], 'rows');
            
            % Draw dots
            if sum(Inside) > 0
                Screen('DrawDots', Win, [X(Inside), Y(Inside)]', Dots.size, Dots.color(Inside, :)', [], Parameters.DotResolution);
            end
        end
        
        %% Draw artificial scotoma
        
        % Only if it's requested
        if ~isempty(ScotomaTexture)
            
            % Draw scotoma mask
            Screen('DrawTexture', Win, ScotomaTexture, ...            
                [], [], [], 0); % FilterMode = 0
        end
        
        %% Draw fixation
        
        % Draw fixation grid
        Screen('DrawTexture', Win, GridTexture, ...
            [], [], [], 0); % FilterMode = 0
        
        % Find the colour of the dot to show this frame
        FixColor = Parameters.TaskColor(Task.EventState(CurrFrame), :);
        
        % Draw fixation point
        Screen('DrawDots', Win, [0, 0], Parameters.FixDotSize, [FixColor, Parameters.FixDotAlpha], CentreDisp, Parameters.DotResolution);
        
        %% Response
        
        % Read button presses
        ButtonState = HemiaMap_Buttonbox(Parameters, 'read');
        
        % Log
        if sum(ButtonState(1:4)) > 0
            Behaviour.Response(end + 1) = find(ButtonState, 1);
            Behaviour.ResponseTime(end + 1) = GetSecs - StartTime;
        end
        
        %% Loop out
        [~, ~, Key] = KbCheck(Parameters.Kb);
        if Key(Parameters.KbEscape)
            disp('%%%% Aborted! %%%%')
            break
        end
        
        %% Flip
        
        % Flip
        Screen('Flip', Win);        
    end
end

% Done
%