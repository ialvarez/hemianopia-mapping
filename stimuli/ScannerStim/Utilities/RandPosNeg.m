function Y = RandPosNeg(varargin)
% Y = RandPosNeg(varargin)
%
% Input
%     RandPosNeg(N) returns a N-by-N matrix 
%     RandPosNeg(M,N) returns a M-by-N matrix
%     RandPosNeg(M,N,P,...) returns a M-by-N-by-P-... matrix
%
% Create a matrix of random -1 and +1 values for the desired size. Emulates
% the functionality of the rand() family of functions.
% 
% Alternatively, use randsample([-1 1], N, true)
%
% Ivan Alvarez
% FMRIB Centre, University of Oxford

%% Main

% Random integers spanning [0 1]
Y = randi([0 1], cell2mat(varargin));

% Replace 0 with -1 
Y(Y == 0) = -1;

% Done
%