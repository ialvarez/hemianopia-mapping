function ButtonState = IOport_read(IO, ioObj, address)
% ButtonState = IOport_read(IO, ioObj, address)
% 
% Read the current state of the buttonbox with Windows IO. FMRIB-specific.
% 
% 22/01/2018
% Ivan Alvarez
% FMRIB Centre, University of Oxford

% Read bytes
byte_in=io64(ioObj, address);

% Go over each button (first byte is inverted)
ButtonState(1) = ~bitget(byte_in, IO.buttonBit(1));
ButtonState(2) = bitget(byte_in, IO.buttonBit(2));
ButtonState(3) = bitget(byte_in, IO.buttonBit(3));
ButtonState(4) = bitget(byte_in, IO.buttonBit(4));
ButtonState(5) = bitget(byte_in, IO.trigBit);

% Done
%