function EyelinkStop(EdfFile, OutputFile)
% EyelinkStop(EdfFile, OutputFile)
% 
% Input:
%   EdfFile      [string] File created by EyelinkStart.m
%   OutputFile   [string] Name for output .edf file (e.g. 'my_results.edf')
%
% Basic script for terminating gaze recording with Eyelink 1000 via PTB.
%
% Changelog
%
% 20/08/2015    Written
% 21/08/2018    Changed output message from TRIALEND to STIMEND
%
% Ivan Alvarez
% FMRIB, University of Oxford

%% Main

% End message
Eyelink('Message', 'STIMEND');

% Stop recording
Eyelink('StopRecording');

% Close graphics window and close data file
Eyelink('CloseFile');
    
% Save file
status = Eyelink('ReceiveFile');
if status > 0
    fprintf('Eyelink ReceiveFile status %d\n', status);
end

% Shutdown Eyelink
Eyelink('Shutdown');

% Check file has been saved correctly
if exist(EdfFile, 'file') == 2
    disp('Eyelink .edf file saved successfully');
else
    warning('Problem receiving data from eyetracker.')
end
    
% Re-name file
movefile(EdfFile, OutputFile);

% Done
%