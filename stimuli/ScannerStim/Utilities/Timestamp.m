function T = Timestamp(Format)
% T = Timestamp([Format])
%
% Input
%       Format     [string] 'Date'     yyyyMMdd      (e.g. 20150101)
%                           'DateTime' yyyyMMdd-HHmm (e.g. 20150101-1010) [default]
%                           'TimeSec'  HH-mm-ss-ms   (e.g. 01-01-01-0001)
% Output
%       T          [string] timestamp
%
% Create a timestamp string for now.

% Changelog
%
% 15/02/2018    Written
% 13/12/2018    Added Date format
%
% Ivan Alvarez
% FMRIB Centre, University of Oxford

% Default
if nargin == 0
    Format = 'DateTime';
end

% Create
switch Format
    case 'Date'
        T = char(datetime('now', 'Format', 'yyyyMMdd'));
    case 'DateTime'
        T = char(datetime('now', 'Format', 'yyyyMMdd-HHmm'));
    case 'TimeSec'
        T = char(datetime('now', 'Format', 'HH-mm-ss-ms'));
end

% Done
%