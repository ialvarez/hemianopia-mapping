function [EdfFile, Handle] = EyelinkStart(Win, Calibration)
% [EdfFile, Handle] = EyelinkStart(Win, Calibration)
% 
% Input:
%   Win            [struct] PTB Window handle
%   Calibration    [scalar] 1 = perform calibration
%                           0 = don't.
%
% Output:
%   EdfFile        [string] file in current directory where the Eyelink data
%                  is saved to. This is a temporary file
%   Handle         [struct] EyeLink structure with settings
%
% Basic script for launching the Eyelink 1000 interface via PTB. Works with
% DPAG stereoscope set-up.
%
% Changelog
%
% 20/08/2015    Written
% 26/08/2015    Added default values
% 04/09/2015    Now requires PTB's window handle as input
% 18/12/2017    Removed drift correction, this can be done in the GUI
% 21/08/2018    Ammended defaults
%
% Ivan Alvarez
% FMRIB, University of Oxford

%% Defaults

% Help message
if nargin == 0
    help EyelinkStart
    return
end

% Default is no calibration
if nargin < 2
    Calibration = 0;
end

%% Main

% Create temporary .edf record file
% Note this file must be a short name with no special characters (only
% numbers and letters)
PID = feature('getpid');
EdfFile = ['tmp' num2str(PID) '.edf'];

% Disable dummy mode
DummyMode = 0;

% Load default settings
Handle = EyelinkInitDefaults(Win);

% Intialisation test
if ~EyelinkInit(DummyMode, 1)
    error('Eyelink Init aborted');
end

% Check which tracker we're using, and display on screen
[~, vs] = Eyelink('GetTrackerVersion');
disp(['Running experiment on a ' vs]);

% What data to gather
Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');

% Open file to record data to
Eyelink('Openfile', EdfFile);

% Calibration
if Calibration
    
    % Calibrate the eye tracker
    EyelinkDoTrackerSetup(Handle);    
end

% Start recording
Eyelink('StartRecording');

% Mark zero-point in data file
Eyelink('Message', 'SYNCTIME'); 

% Indicate stimulation starts now
Eyelink('Message', 'STIMSTART');

% Done
%