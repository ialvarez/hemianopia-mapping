function Y = RandomiseNoRepeats(X, Blocks)
% Y = RandomiseNoRepeats(X, Blocks)
%
% Input
%       X      [vector] Input series. Can contain repeats
%       Blocks [scalar] Number of iterations to randomise from X
%
% Output
%       Y      [matrix] Blocks x m matrix, where each row is a permutation
%                       of X with no repeated values.
%
% Create a matrix [Y] of a set of items [X] in randomised order, ensuring
% no two adjacent pairs of numbers are indentical (i.e. no neighbouring
% repeats). 
% Multiple permutations can be requested with Blocks, with each block
% checked to avoid the first item being the same as the last item of the
% previous block. Note the same within-block permutations can occur more 
% than once across blocks.
% 

%% Changelog
% 07/01/2018    Written
%
% Ivan Alvarez
% FMRIB Centre, University of Oxford

%% Main

% Defaults
if nargin < 2
    Blocks = 1;
end

% Pre-assign output variable
Y = nan(Blocks, length(X));

% How many unique elements
U = unique(X);

%% Unique items

% If all items are unique, use randomised sampling
if length(U) == length(X)
   
    % Loop blocks
    for b = 1:Blocks
        
        % Pull a random permutation
        Y(b, :) = X(randperm(length(X)));
        
        % Ensure the first item of the new entry is not a repeat of the last
        % item of the previous entry
        if b > 1
            while Y(b, 1) == Y(b - 1, end)
                Y(b, :) = X(randperm(length(X)));
            end
        end
    end
end

%% Non-unique items

% If we have non-unique entries in X, find all permutations with no repeats
if length(U) < length(X)

    % If the input vector is very long, throw a warning
    if length(X) > 10
        error('More than 10 items in X with non-unique values, too many permutations to calculate.')
    end
    
    % All possible permutations
    P = perms(X);

    % Start index of good pemutations
    GoodPer = zeros(size(P, 1), 1);
    
    % Loop permutations
    for i = 1:size(P, 1)

        % Check every pair of values
        for j = 1:size(P, 2) - 1
            Check(j) = P(i, j) == P(i, j + 1);
        end
        
        % If no repeats found, classify as a good permutation
        if sum(Check) == 0
            GoodPer(i) = 1;
        end        
    end
    
    % Mask permutations
    P = P(logical(GoodPer), :);
    
    % Check at least one permutation survives
    if isempty(P)
        error('No valid permutations found.')
    end

    % Check that all permutations don't start and end on the same number
    if length(unique([P(:, 1); P(:, end)])) < 2
        error('All available permutation start & end with the same value.')
    end
    
    % Loop blocks
    for b = 1:Blocks
        
        % Pull a random permutation
        Y(b, :) = P(randi(size(P, 1), 1), :);
        
        % Ensure the first item of the new entry is not a repeat of the last
        % item of the previous entry
        if b > 1
            while Y(b, 1) == Y(b - 1, end)
                Y(b, :) = P(randi(size(P, 1), 1), :);
            end
        end
    end
end

% Done
%