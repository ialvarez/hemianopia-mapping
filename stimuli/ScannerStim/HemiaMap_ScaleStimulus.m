%% HemiaMap_ScaleStimulus.m
%
% This does three things:
%   1. Resize the HCP stimulus files to the desired resolution
%   2. Resize and restructure apertures to separate wedge/ring from bars
%   3. Create an index of the type of aperture shown at each frame
%
%This will create three files:
%   barmasks.mat - apertures for bar stimulation
%   ridgemasks.mat - apertures for wedge/ring stimulation
%   fixationgrid.mat - single RGBA frame with the fixation grid
%   patterns.mat - movie frames for retino stimulus
%
% CHANGELOG
% 29/11/2018    Written
% 03/12/2018    Create and save aperture index (ApIdx)
% 06/02/2019    Now loads the stimulus directory from global parameters
%
% Ivan Alvarez
% FMRIB, University of Oxford
%

%% Settings

% Resolution
% Laptop (Nautilus): 1280 x 800
% FMRIB desktop (Bay): 1920 x 1080
% FMRIB 3T (BoldScreen): 1920 x 1200
Resolution = [1280, 800];

%% Parameters

% Pull global parameters
Parameters = HemiaMap_Global;

%% Loading

% Message
disp('Loading...')

% Load original files
% This contains the following variables;
%   * masks             (binary masks for wedges, rings, and bars)
%   * multibarindices   (positions for bar apertures)
%   * wedgeringindices  (positions for wedges and ring apertures)
%   * patterns          (movie frames)
%   * specialoverlay    (the fixation grid)
load([Parameters.StimDir '/original/stimuli.mat']);
load([Parameters.StimDir '/original/fixationgrid.mat']);

%% Scaling

% Message
disp('Resizing...')

% Find the maximum diameter display available, typically the vertical
% resolution
Diameter = min(Resolution);

% Interpolate to maximum box size
masks = imresize(masks, [Diameter, Diameter]);
patterns = imresize(patterns, [Diameter, Diameter]);
specialoverlay = imresize(specialoverlay, [Diameter, Diameter]);

%% Bar apertures

% Message
disp('Making bar apertures...')

% Empty matrix for bar apertures
Bar = zeros(Diameter, Diameter, length(multibarindices), 'uint8');

% Pull bar apertures
for i = 1:length(multibarindices)
    if multibarindices(i) ~= 0
        Bar(:, :, i) = masks(:, :, multibarindices(i));
    end
end

% Which dimension to pad (typically X)
Dim = find(Resolution ~= Diameter);

% Pad with zeros
PadSize = abs(diff(Resolution)) / 2;
if Dim == 1
    Bar = padarray(Bar, [0, PadSize, 0], 0);
elseif Dim == 2
    Bar = padarray(Bar, [PadSize, 0, 0], 0);    
end

% Create aperture index
% 1 = LR
% 2 = UD
% 3 = Diag1 (bottom left to upper right)
% 4 = Diag2 (bottom right to upper left)
BarIdx = multibarindices;
BarIdx(BarIdx >= 901 & BarIdx <= 1320) = 1;
BarIdx(BarIdx >= 1741 & BarIdx <= 2160) = 2;
BarIdx(BarIdx >= 1321 & BarIdx <= 1740) = 3;
BarIdx(BarIdx >= 2161 & BarIdx <= 2580) = 4;

%% Wedge/ring apertures

% Empty matrix for wedge/ring apertures
Wedge = zeros(Diameter, Diameter, length(wedgeringindices), 'uint8');

% Pull wedge/ring apertures
for i = 1:length(wedgeringindices)
    if wedgeringindices(i) ~= 0
        Wedge(:, :, i) = masks(:, :, wedgeringindices(i));
    end
end

% Which dimension to pad (typically X)
Dim = find(Resolution ~= Diameter);

% Pad with zeros
PadSize = abs(diff(Resolution)) / 2;
if Dim == 1
    Wedge = padarray(Wedge, [0, PadSize, 0], 0);
elseif Dim == 2
    Wedge = padarray(Wedge, [PadSize, 0, 0], 0);    
end

% Create aperture index
% 1 = Wedge
% 2 = Ring
WedgeIdx = wedgeringindices;
WedgeIdx(WedgeIdx >= 1 & WedgeIdx <= 480) = 1;
WedgeIdx(WedgeIdx >= 481 & WedgeIdx <= 900) = 2;

%% Save

% Message
disp('Saving...')

% Suffix for save files depends on resolution
Suffix = [num2str(Resolution(1)) '_' num2str(Resolution(2))];

% Bar apertures
ApFrm = Bar;
ApIdx = BarIdx;
save([Parameters.StimDir '/barmasks_' Suffix '.mat'], 'ApFrm', 'ApIdx', '-v7.3');

% Wedge/ring apertures
ApFrm = Wedge;
ApIdx = WedgeIdx;
save([Parameters.StimDir '/ridgemasks_' Suffix '.mat'], 'ApFrm', 'ApIdx', '-v7.3');

% Fixation grid
fixationgrid = specialoverlay;
save([Parameters.StimDir '/fixationgrid_' Suffix '.mat'], 'fixationgrid', '-v7.3');

% movie frames
save([Parameters.StimDir '/patterns_' Suffix '.mat'], 'patterns', '-v7.3');

% Message
disp('Done.')

% Done
%