function Behaviour = HemiaMap_RetinoBars(Parameters, Win, Task)
%  Behaviour = HemiaMap_RetinoBars(Parameters, Win, Task)
%
% Inputs
%   Parameters    [struct] As generated by HemiaMap_Global
%   Win           [scalar] From PTB window launch
%   Task          [struct] As generated by HemiaMap_GenTask
%
% Display HCP 7T retinotopy stimulus, only the bar apertures.
%
% Stimulus data from http://kendrickkay.net/analyzePRF
%
% CHANGELOG
% 27/11/2018    Written
% 29/11/2018    Implemented aperture loading
% 30/11/2018    Updated HemiaMap_LoadStim output order
%               Resolution now stored in Parameters
% 04/12/2018    Moved buttonbox interactions to HemiaMap_Buttonbox
% 13/12/2018    Re-wrote frame timekeeping, now handles dropped frames
% 07/01/2019    Added artificial scotoma
% 11/02/2019    Scotoma mask now extends to the lateral edge of the screen
%               Welcome text goes yellow when ready to trigger
% 12/02/2019    Fixed scotoma mask bug, was not extending fully
% 22/02/2019    Only draw a frame if the expected framerate has elapsed,
%               fixes jitter-y display in STIM1
% 05/03/2019    Set DrawTexture FilterMode to 0 for faster drawing
%               Moved Flip() call to the end of the loop for less overhead
%
% Ivan Alvarez
% FMRIB, University of Oxford
%

%% Settings

% How long to show each frame for, in seconds
FrameDur = (1 / Parameters.RefreshRate) * Parameters.FrameDuration;

% Experiment length
Fps = Parameters.RefreshRate / Parameters.FrameDuration; % Frames per second
TotalDur = Parameters.Nframes / Fps; % Stimulus duration, in seconds

% Centre of display
CentreDisp = Parameters.Resolution / 2;

% Make a baseline RGB gray image
Im = permute(Parameters.Gray * 255, [1 3 2]);
Im = repmat(Im, Parameters.Resolution(2), Parameters.Resolution(1));
Im = uint8(Im);

%% Load

% Message
disp('Start: Loading apertures...')

% Load (takes 30 seconds)
[Grid, ApFrm, ApIdx, Movie] = HemiaMap_LoadStim(Parameters);

% Invert apertures
ApFrm = imcomplement(ApFrm);

%% Make textures

% Message
disp('Start: Making textures')

% Grid texture
GridTexture = Screen('MakeTexture', Win, Grid);

% Preload movie frames as textures
for i = 1 : size(Movie, 4)
    MovieTexture(i) = Screen('MakeTexture', Win, Movie(:, :, :, i));
end

% Movie texture index. Repeat the sequence until we cover all frames
MovieIdx = 1 : size(Movie, 4);
MovieIdx = repmat(MovieIdx, [1, ceil(Parameters.Nframes / length(MovieIdx))]);

% Artificial scotoma image
M = permute(Parameters.Gray, [3 1 2]);
M = repmat(M, Parameters.Resolution(2), Parameters.Resolution(1));
M(:, :, 4) = 0;
M(:, 1 : size(M, 2) / 2, 4) = 1;

% Artifical scotoma texture
switch Parameters.Scotoma
    case 'No'
        ScotomaTexture = [];
    case 'LVF'
        ScotomaTexture = Screen('MakeTexture', Win, M);
    case 'RVF'
        ScotomaTexture = Screen('MakeTexture', Win, fliplr(M));
end

%% Behaviour record

% Start empty matrix
Behaviour.EventTime = Task.EventTime;
Behaviour.Response = [];
Behaviour.ResponseTime = [];

%% Ready screen

% Draw grid
Screen('DrawTexture', Win, GridTexture);

% Draw ready text
DrawFormattedText(Win, Parameters.ReadyMessage, 'center', 'center', Parameters.White);

% Draw fixation point
Screen('DrawDots', Win, [0, 0], Parameters.FixDotSize, [Parameters.Black, Parameters.FixDotAlpha], Parameters.Resolution / 2, Parameters.DotResolution);

% Flip
Screen('Flip', Win);

%% Wait for trigger

% Message
disp('%%%% Waiting for trigger %%%%')

% Wait for button 5
HemiaMap_Buttonbox(Parameters, 'waitfortrigger');

% Message to EyeLink
if Parameters.Et
    Eyelink('Command', 'record_status_message ''SCANNER_TRIGGER''');
    Eyelink('Message', 'SCANNER_TRIGGER');
end

%% Display

% Message
disp('%%%% Displaying %%%%')

% Clocks
StartTime = GetSecs;
CurrTime = GetSecs - StartTime;

% Frames
FrameTimes = FrameDur : FrameDur : TotalDur;
LastFrame = 0;
CurrFrame = 0;

% Loop
while CurrTime < TotalDur
    
    %% Clocks
    
    % Update clocks
    CurrTime = GetSecs - StartTime;
    
    % Find nearest frame
    [~, CurrFrame] = min(abs(FrameTimes - CurrTime));
    
    %% Draw if we have a new frame
    
    % Check if we need to draw a new frame
    if CurrFrame > LastFrame
    
        % Update frame counter
        LastFrame = CurrFrame;
        
        %% Background
        
        % Grey background
        Screen('FillRect', Win, Parameters.Gray);
        
        %% Draw movie
        
        % Only if its a stimulation frame
        if ApIdx(CurrFrame) ~= 0
            
            % Make aperture texture, send to alpha layer
            Im(:, :, 4) = ApFrm(:, :, CurrFrame);
            ApertureTexture = Screen('MakeTexture', Win, Im);
            
            % Draw movie
            Screen('DrawTexture', Win, MovieTexture(MovieIdx(CurrFrame)), ...
                [], [], [], 0); % FilterMode = 0
            
            % Draw aperture
            Screen('DrawTexture', Win, ApertureTexture, ...
                [], [], [], 0); % FilterMode = 0
        end
        
        %% Draw artificial scotoma
        
        % Only if it's requested
        if ~isempty(ScotomaTexture)
            
            % Draw scotoma mask
            Screen('DrawTexture', Win, ScotomaTexture, ...
                [], [], [], 0); % FilterMode = 0
        end
        
        %% Draw fixation
        
        % Draw fixation grid
        Screen('DrawTexture', Win, GridTexture, ...
            [], [], [], 0); % FilterMode = 0

        % Find the colour of the dot to show this frame
        switch Parameters.Task
            case 'flash'
                FixColor = Parameters.TaskColor(Task.EventState(CurrFrame), :);
            case 'swap'
                FixColor = Parameters.TaskColor(Task.EventState(CurrFrame), :);
        end
        
        % Draw fixation point
        Screen('DrawDots', Win, [0, 0], Parameters.FixDotSize, [FixColor, Parameters.FixDotAlpha], CentreDisp, Parameters.DotResolution);
        
        %% Response
        
        % Read button presses
        ButtonState = HemiaMap_Buttonbox(Parameters, 'read');
        
        % Log
        if sum(ButtonState(1:4)) > 0
            Behaviour.Response(end + 1) = find(ButtonState, 1);
            Behaviour.ResponseTime(end + 1) = GetSecs - StartTime;
        end
        
        %% Loop out
        [~, ~, Key] = KbCheck(Parameters.Kb);
        if Key(Parameters.KbEscape)
            disp('%%%% Aborted! %%%%')
            break
        end
        
        %% Flip
        
        % Flip
        Screen('Flip', Win);               
    end
end

% Done
%