#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from math import atan2
from math import pi
import os
import re
import sys

subject = sys.argv[1]

files = os.listdir('../data/rawdata/conditions')
datasets = []
for file in files:
    if len(re.findall(".*\.csv",file))>0:
        temp = re.split("_|\.",file)
        subject_file = temp[1]
        if subject_file == subject:
            location = temp[-2]
            condition = temp[0]
            hemisphere = temp[2]
            exec(subject + '_' + location + ' = pd.read_csv(\'../data/rawdata/conditions/' + file + '\')')
            exec(subject + '_' + location +'[\'Eccentricity\'] = pd.Series(np.array([np.sqrt(' + subject + '_' + location + '.iloc[i].x0**2 + ' + subject + '_' + location + '.iloc[i].y0**2) for i in ' + subject + '_' + location + '.index]), index=' + subject + '_' + location + '.index)')
            exec(subject + '_' + location +'[\'Polar\'] = pd.Series(np.array([atan2(' + subject + '_' + location + '.iloc[i].x0, ' + subject + '_' + location + '.iloc[i].y0) / (pi*180) for i in ' + subject + '_' + location + '.index]), index=' + subject + '_' + location + '.index)')
            exec(subject + '_' + location +'[\'Location\']=\''+ location+'\'')
            exec(subject + '_' + location +'[\'Subject\']=\''+ subject +'\'')
            exec(subject + '_' + location +'[\'Condition\']=\'' + condition + '\'')
            exec(subject + '_' + location +'[\'Hemisphere\']=\'' + hemisphere + '\'')
            exec('datasets.append('+ subject + '_' + location + ')')
    
data = pd.concat(datasets)

exec('data.to_csv(\'../data/combined/' + subject + '_conditions.csv\',index =False)')

files = os.listdir('../data/rawdata/average')
datasets = []
for file in files:
    if len(re.findall(".*\.csv",file))>0:
        temp = re.split("_|\.",file)
        subject_file = temp[0]
        if subject_file == subject:
            location = temp[-2]
            hemisphere = temp[1]
            exec(subject + '_' + location + ' = pd.read_csv(\'../data/rawdata/average/' + file + '\')')
            exec(subject + '_' + location +'[\'Eccentricity\'] = pd.Series(np.array([np.sqrt(' + subject + '_' + location + '.iloc[i].x0**2 + ' + subject + '_' + location + '.iloc[i].y0**2) for i in ' + subject + '_' + location + '.index]), index=' + subject + '_' + location + '.index)')
            exec(subject + '_' + location +'[\'Polar\'] = pd.Series(np.array([atan2(' + subject + '_' + location + '.iloc[i].x0, ' + subject + '_' + location + '.iloc[i].y0) / (pi*180) for i in ' + subject + '_' + location + '.index]), index=' + subject + '_' + location + '.index)')
            exec(subject + '_' + location +'[\'Location\']=\''+ location+'\'')
            exec(subject + '_' + location +'[\'Subject\']=\''+ subject +'\'')
            exec(subject + '_' + location +'[\'Hemisphere\']=\'' + hemisphere + '\'')
            exec('datasets.append('+ subject + '_' + location + ')')
    
data = pd.concat(datasets)

exec('data.to_csv(\'../data/combined/' + subject + '_average.csv\',index =False)')